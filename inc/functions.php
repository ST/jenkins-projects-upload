<?php
/**
 * User: partimo
 * Date: 15/02/16
 * Time: 11:15
 */


function is_function($f) {
    return (is_string($f) && function_exists($f)) || (is_object($f) && ($f instanceof Closure));
}

function show_no_path_error($data) {
    $data['tpl.data']['greeting'] = "There was an error";
    $data['tpl.data']['template'] = 'login';
    $data['tpl.data']['title'] = 'Login with your SIB userid';


    if(!isset($data['tpl.data']['loginmessage'])) {
        $data['tpl.data']['loginreason'] = "Page Not Found";
    }

    return $data;
}

function destroy_session(){


    // Initialize the session.
    session_start();

    // Unset all of the session variables.
    $_SESSION = array();

    // If it's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    // Finally, destroy the session.
    session_destroy();

}

/*
 * check if the directories required by Smarty template engine exists and create them if needed
 * @param array smarty directories
 * @return TRUE if all required directories are a OK
 */
function check_dirs($app){

    if(!is_array($app['settings']['tpl']) || !isset($app['settings']['log_dir'])){
        \FB::error($app, "app error in ".__FUNCTION__);
        die("required path setting is missing");

    }

    define('TPL_DIR', $app['settings']['tpl']['template_dir']);
    define('TPL_DIR_C', $app['settings']['tpl']['compile_dir']);
    define('TPL_CONFIGS', $app['settings']['tpl']['config_dir']);
    define('TPL_CACHE', $app['settings']['tpl']['cache_dir']);
    define('LOG_DIR', $app['settings']['log_dir']);

    if(!file_exists(TPL_DIR_C)){

        if(!mkdir(TPL_DIR_C, 0770, TRUE)){
            $msg = 'Compile directory: '.TPL_DIR_C.' hasn\'t been created.<br />You can fix this error by
            doing:<br>"chmod
            777 templates" on the main directory<br>then reload this page and then do "chmod 755 templates"';
            FB::error($msg);
            die($msg);
        }

    }

    if(!is_writable(TPL_DIR_C)){
        $msg = 'Compile dir: '.TPL_DIR_C.' directory is not writable, please correct the permission (chmod 770 '.__dir__.'/'.TPL_DIR_C.')';
        FB::error($msg);
        die($msg);
    }

    if(!file_exists(TPL_CACHE)){
        if(!mkdir(TPL_CACHE, 0770, TRUE)){
            $msg = 'cache directory has not been created or it is not writable.<br /><br />You can fix this error by doing "chmod
            777 templates" on the main directory, reload this page and then do "chmod 755 templates".';
            FB::error($msg);
            die($msg);
        }

    }

    if(!is_writable(TPL_CACHE)){
        $msg = 'Cache directory: '.TPL_CACHE.' is not writable, please correct the permissions (chmod 770 '.__dir__.'/'.TPL_CACHE.')';
        FB::error($msg);
        die($msg);
    }

    if(!file_exists(TPL_CONFIGS)){
        if(!mkdir(TPL_CONFIGS, 0770, TRUE)){
            $msg = 'configs directory '.TPL_CONFIGS.' has not been created or it is not writable.<br /><br />You can fix this
            error by
            doing "chmod 777 templates" on the main directory, reload this page and then do "chmod 755 templates".';
            FB::error($msg);
            die($msg);
        }

    }

    if(!is_writable(TPL_CONFIGS)){
        $msg = 'Configs directory: '.TPL_CONFIGS.' directory is not writable, please correct the permissions (chmod 770 '.__dir__.'/'.TPL_CONFIGS.')';
        FB::error($msg);
        die($msg);
    }


    if(!file_exists(LOG_DIR) || !is_writable(LOG_DIR)){
        $msg = 'log directory '.LOG_DIR.' has not been created or it is not writable.<br /><br />You can fix this error by creating it and then giving webserver the right to write to it.';
        FB::error($msg);
        die($msg);

    }

    if(!is_writable(TPL_CACHE)){
        $msg = 'Cache directory: '.TPL_CACHE.' is not writable, please correct the permissions (chmod 770 '.__dir__.'/'.TPL_CACHE.')';
        FB::error($msg);
        die($msg);
    }


    if(is_writable(TPL_DIR)){
        FB::warn("templates directory seems to be writable, please do 'chmod 755 templates' to fix potential security risk.");
    }



    return true;

}

function redirect($url, $statusCode = 303){
    header('Location: ' . $url, true, $statusCode);
    die();
}

function is_user_authenticated($app){

    $auth = new Vital\Auth\Auth("ldap", $app['settings'], $app['logger']);

    FB::info($_SESSION, "session in ".__FUNCTION__);

    if(isset($_SESSION['authenticated']) && $_SESSION['authenticated']) {
        FB::info("user is authenticated");
        return true;
    }

    $params = array_map('strip_tags', $_POST);
    if(!is_array($app['params'])){ $app['params'] = array(); }
    $app['params'] = array_merge($params, $app['params']);
    FB::info($app, "app in ".__FUNCTION__);

    if(isset($params['username']) && isset($params['pw'])) {

        FB::info("app is protected and username: " . $params['username'] . " pw: ".substr($params['pw'], 0, 2)."... given, try to login");
        $authenticated = $auth->Authenticate($app['params']);

    } else {
        FB::error("app is protected and either username: " . $params['username'] . " or pw: ".substr($params['pw'], 0, 2)."... was missing. trying again.");
        $authenticated = false;
    }

    return $authenticated;

}

function no_auth_required($app, $pagename) {
    FB::info($app['settings'], "app settings in ".__FUNCTION__);
    FB::info($pagename, "pagename settings in ".__FUNCTION__);
    return in_array($pagename, $app['settings']['allowed']);
    
}
