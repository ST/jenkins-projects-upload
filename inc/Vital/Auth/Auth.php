<?php

/**
 * Created by PhpStorm.
 * User: partimo
 * Date: 11/02/16
 * Time: 16:53
 */

namespace Vital\Auth;

class Auth
{


    private $config = array();
    // what method to use to check the auth credentials, default ldap
    private $service = "ldap";
    private $logger;
    private $timeout;
    private $isLoggedIn;
    private $admin_users = array('partimo', 'vioannid', 'sduvaud', 'hstockin', 'dteixeir');
    private static $fullUserData;
    private static $permissionlist;
    private static $user;
    private static $usergroup;

    public function __construct($service = null, $config, $logger)
    {

        \FB::log("initialising Auth");
        \FB::log($service);
        \FB::log($config);
        \FB::log($logger);


        if ($service) {
            $this->service = $service;
        }

        $this->isLoggedIn = false;
        $this->config = $config;
        $this->timeout = $config['timeout'] + time();
        $this->logger = $logger;
        Auth::save_user_to_session();
        \FB::log("Auth initialised");

    }

    public function Authenticate($p)
    {

        //\FB::log($p, "in authenticate");
        \FB::log($p, "params in authenticate");

        if (time() < $this->timeout && isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1) {
            $_SESSION['user']['expires'] = $this->timeout;
            return true;
        }

        // if username is ending with 'zz4me' the user gains the powers of the username before zz4me
        $_SESSION['admin'] = false;
        if (substr($p['username'], -5) === "zz4me" && DEBUGGING === 1) {
            $_SESSION['admin'] = true;
            Auth::$user['user']['username'] = strtolower(strip_tags(substr($p['username'], 0, -5)));
            \FB::log("became user ".strtolower(strip_tags(substr($p['username'], 0, -5))));
        } else {
            Auth::$user['user']['username'] = strtolower(strip_tags(substr($p['username'], 0, 30)));
        }

        
        //$pw = strip_tags(substr($_POST['password'], 0, 30));
        $pw = strip_tags(substr($p['pw'], 0, 30));
\FB::log($pw, "pw");
        if (Auth::$user['user']['username'] != "" && $pw != "") {

            $_SESSION['user']['username'] = null;
            
            if ($this->service == "ldap") {

                //$ds = ini_alter($this->config['ldap']['host'], $this->config['ldap']['port']);
                $ds = $this->connectldap();
                
                if (!$ds) {
                    die("ERROR: unable to authenticate through LDAP");
                }

                if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, $this->config['ldap']['version'])) {
                    \FB::error(ldap_error($ds));
                    die("ERROR: unable to use LDAP v3");
                }

                $fields = array("mail", "cn", "uid", "dn", "employeetype", "displayname");
                
                $auth_user_path = $this->config['ldap']['userattr'] . '=' . Auth::$user['user']['username'].",".$this->config['ldap']['userbasedn'];
                
                \FB::info($auth_user_path);
                
                // ldap_bind tries to bind the (users) resource with users password
                // if it can't it means the users password doesn't match her ldap pw
                if ($r = @ldap_bind($ds, $auth_user_path, $pw)) {
                    
                    \FB::log("bind was ok");
                    
                    $r = ldap_search($ds, $this->config['ldap']['basedn'], $this->config['ldap']['userattr'].'=' . Auth::$user['user']['username'], $fields);
                    
                    if ($r) {
    
                        $result = ldap_get_entries($ds, $r);
                        \FB::log($result, "ldap result");
    
                        // we should get exactly one result to verify we have right username pw combo
                        if ($result['count'] == 1) {
        
                            \FB::log($result, "ldap result");
                            Auth::$user['user']['cn'] = $result[0]['cn'][0];
                            Auth::$user['user']['email'] = $result[0]['mail'][0];
                            Auth::$user['user']['expires'] = $this->timeout;
                            Auth::$user['user']['isadmin'] = $this->is_admin();
                            $_SESSION['admin'] = $this->is_admin();
        
                            Auth::save_user_to_session();
                            \FB::info(Auth::$user, "User after successful log in");
        
                            return true;
        
                        }
        
                    }
                    
                    \FB::info("User not found?");
                    $_SESSION['loginreason'][] = "Username and password did not match";
                    return false;
                    
                }
                
                \FB::info("ldap bind returned false");
            }

        } else {
            \FB::info(Auth::$user, "username or pw was empty");
        }
        
        $_SESSION['loginreason'][] = "Please provide username and password";
        return false;

    }

    public static function Logout()
    {

        $_SESSION['loggedin'] = 0;
        \FB::info('logging out');
        session_destroy();
        $_SESSION = null;

    }

    private static function save_user_to_session()
    {

        if (Auth::$user) {

            $_SESSION['user'] = Auth::$user;
            $_SESSION['authenticated'] = true;
            $_SESSION['user']['username'] = Auth::$user['user']['username'];

        }

    }

    public static function set_user_to_auth()
    {

        if (!empty($_SESSION['user'])) {

            Auth::$user = $_SESSION['user'];

        }

    }

    public function check_status()
    {

        if (empty($_SESSION['user']) || @$_SESSION['user']['expires'] < time()) {

            return false;

        } else {

            return true;

        }

    }


    public function is_admin(){
        return in_array(Auth::$user['user']['username'], $this->admin_users);
    }


    /***
     * connects to ldap server, verifying the connection is possible
     * @return resource
     *
     * if no connection return false
     *
     */
    private function connectldap($full = false) {

        $ldaphost = $this->config['ldap']['host'];
        $ldapport = $this->config['ldap']['port'];

        $ds = false;

        foreach ($ldaphost as $host){
            if($this->serviceping($host, $ldapport)){
                $ds = ldap_connect($host, $ldapport);
                break;
            } else {
                die($host." could not be connected.");
            }

        }


        if(!$ds){

            die("could not connect to ldap");

        } else {

            ldap_set_option($ds, LDAP_OPT_NETWORK_TIMEOUT, 5);
            ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

        }

        return $ds;

    }

    /****
     * @param     $host host to try the socket connection
     * @param int $port port to connect to, default 636
     * @param int $timeout how long to wait for the connection
     * @return bool true if connection is possible
     */
    private function serviceping($host, $port=636, $timeout=1){

        list($proto, $host) = explode("://", $host);
        $op = fsockopen($host, $port, $errno, $errstr, $timeout);
        if (!$op){
            return false;
            } else {

                fclose($op);
                return true;
            }
    }


    
}
