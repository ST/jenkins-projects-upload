<?php
/**
 * User: partimo
 * Date: 19/04/16
 */

function admin($app){
    FB::log("in ".__FUNCTION__);
    FB::info($app['settings']['uploaddir']);

    $local_test_upload_dir = $app['settings']['uploaddir'];

    $data = array();

    $data['tpl.data']['testdir'] = $app['settings']['uploaddir'];

    if(!$_SESSION['admin']) {
        $data['tpl.data']['greeting'] = "Protected page";
        $data['tpl.data']['template'] = 'login';
        $data['tpl.data']['title'] = 'Login with your SIB userid';
        $data['tpl.data']['loginreason'] = "Page is protected.";
        return $data;
    }

    //\FB::log($app['params'], "params1");

    $params = array_map('strip_tags', $_POST);
    $app['params'] = array_merge($params, $app['settings']);

    FB::log($app['params'], "app params");

    if(isset($params['dir'])) {
        $local_test_upload_dir .= $params['dir'];
        $php_files = file_list($local_test_upload_dir, "php");
        $local_test_dir = $app['params']['testsdir'].$params['dir'];
    }

    FB::info($local_test_upload_dir, "upload dir in ".__FUNCTION__);
    FB::info($local_test_dir, "git tests dir in ".__FUNCTION__);
    FB::info($php_files, "files in ".$local_test_upload_dir);

    if(isset($params['file'])){
        
        if($params['file'] != "approve"){
    
            $file = $params['file'];
            FB::info("getting: ".$local_test_upload_dir."/".$file);
            $filecontent = file_get_contents($local_test_upload_dir."/".$file);
            $data['tpl.data']['filecontent'] = htmlspecialchars($filecontent);
            
        } else {

            if(!is_dir($local_test_dir) && !mkdir($local_test_dir, 0777)){

                $messages['errormessages'][] = "test dir ".$local_test_dir." was missing and could not be created";

            } else {


                //$git_cmd = 'cd ' . $local_test_dir . '; git pull;';
                $git_cmd = 'cd /home/local/jenkins/jenkins-projects; git pull;';

                FB::info($git_cmd);

                $git_output = exec($git_cmd, $output, $status);

                FB::info($git_output);
                FB::info($output);

                // mv to jenkins-projects tests dir
                $move_cmd = '\mv -f '.$local_test_upload_dir.'/* '.$local_test_dir;

                FB::info($move_cmd, "move command");

                $mv_output = exec($move_cmd, $output, $status);
                FB::info($mv_output);
                FB::info($output);

                if (0 != $status) {
                    $messages['errormessages'][] = "Moving test " . $local_test_upload_dir . " to " . $local_test_dir . " was unsuccessful, maybe a permission problem?";
                    FB::error($status);
                    FB::error($output);
                } else {

                    $messages['successmessages'][] = "Moved test " . $local_test_upload_dir . " to " . $local_test_dir;
                    $messages['success'] = true;

                }

                //$git_cmd = 'cd ' . $local_test_dir . '; git add .;git commit -m "update of '.$app['params']['dir'].' by ' . $_SESSION['user']['user']['cn'] . '";git push origin master -f;';
                //$git_cmd = 'cd ' . $local_test_dir . '; git add .;git commit -m "update of '.$app['params']['dir'].' by ' . $_SESSION['user']['user']['cn'] . '";git push;';
                $git_cmd = 'cd ' . $app['params']['testsdir'] . '; git add .;cd ' . $local_test_dir . '; git add .;git commit -m "update of '.$app['params']['dir'].' by ' . $_SESSION['user']['user']['cn'] . '";git push;';

                FB::info($git_cmd);

                $git_output = exec($git_cmd, $output, $status);

                FB::info($git_output);

                if (0 != $status) {
                    $messages['errormessages'][] = "committing and pushing " . $local_test_dir . " was unsuccessful";
                    FB::error($status);
                    FB::error($output);
                } else {

                    $messages['successmessages'][] = "Committed and pushed " . $local_test_dir;
                    $messages['success'] = true;

                }

            }

            // we want to show the admin template anyway
            // TODO add messages to admin template
            $data['tpl.data']['template'] = "admin";

            $data['tpl.data']['errors'] = $messages['errormessages'];
            $data['tpl.data']['success'] = $messages['successmessages'];

            return $data;

        }

    }

    $dirlist = dir_list($local_test_upload_dir);
    FB::info($dirlist, "from dir_list");

    $dir_index = 0;

    foreach ($dirlist as $project_dir){
        
        $php_files[$project_dir] = file_list($project_dir, "php");

        FB::info($php_files[$project_dir], "test files for ".$project_dir);

        if(is_array($php_files[$project_dir])){
            foreach($php_files[$project_dir] as $php_file){
                $data['tpl.data']['projects'][$dir_index]['files'][] = $php_file;
            }
        }

        $project_path = explode("/", $project_dir);
        $project = end($project_path);
        $data['tpl.data']['projects'][$dir_index]['id'] = $project_path;
        $data['tpl.data']['projects'][$dir_index]['name'] = $project;
        $dir_index++;
    }

    FB::info($data['tpl.data']['projects']);
    FB::info($php_files);

    $data['tpl.data']['template'] = "admin";
    return $data;

}

function file_list($dir, $ext){

    FB::info("getting ".$ext." files from ".$dir);

    $files = glob($dir."/*.".$ext);

    // get only file to filelist array
    foreach($files as $file){
        $filename_path = explode("/", $file);
        $filename = end($filename_path);
        $filelist[] = $filename;
    }

    FB::info($filelist, "got filelist");
    return $filelist;
}


//array of directories
function dir_list($dir){
    FB::info("getting files/dirs from ".$dir);
    $dirlist = glob($dir."*", GLOB_ONLYDIR);
    FB::info($dirlist, "got dirlist");
    return $dirlist;
}
