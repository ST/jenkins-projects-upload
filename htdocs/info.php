<?php

use Vital\Utility\opaqueencoder\OpaqueEncoder;

require_once __DIR__.'/../vendor/autoload.php';

$string = "partimo";
//$string = "a";

$zz = stringHash($string);
echo 'encode: ',$zz,'\n',PHP_EOL;

$yy = stringHash($zz);
echo 'decode: ',$yy,'<br/>',PHP_EOL;

$encoder = new \Vital\Utility\opaqueencoder\OpaqueEncoder(0x107FCB, OpaqueEncoder::ENCODING_BASE64);
// get hash
$hash = $encoder->encode($zz);
echo "\nhashed: ".$hash;
echo "\nde-hashed: ".$encoder->decode($hash);
echo "\noriginal: ".stringHash($encoder->decode($hash));


/*
 * PHP mcrypt - Basic encryption and decryption of a string
 */
$secret_key = "lausanne_2_basel";

// Create the initialization vector for added security.
$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);

// Encrypt $string
$encrypted_string = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secret_key, $string, MCRYPT_MODE_CBC, $iv);

// Decrypt $string
$decrypted_string = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secret_key, $encrypted_string, MCRYPT_MODE_CBC, $iv);


echo "\nOriginal string : " . $string . "\n";
echo "\nEncrypted string : " . $encrypted_string . "\n";
echo "\nDecrypted string : " . $decrypted_string . "\n";

function stringHash($string){
    $encoded = "";

    if(is_numeric($string)) {
        foreach(str_split($string, 2) as $i){
            $encoded .= toChar($i);
        }
    } else {

        foreach(str_split($string) as $c){
            $encoded .= toNumber($c);
        }

    }

    return $encoded;
}

function toChar($dest)
{
    if ($dest) {
        return chr($dest + 87);
    } else {
        return "";
    }

}

function toNumber($dest)
{
    if ($dest) {
        //make a = 10
        return ord(strtolower($dest)) - 87;
    } else {
        return 0;
    }

}

exit;
$indicesServer = array('PHP_SELF',
    'argv',
    'argc',
    'GATEWAY_INTERFACE',
    'SERVER_ADDR',
    'SERVER_NAME',
    'SERVER_SOFTWARE',
    'SERVER_PROTOCOL',
    'REQUEST_METHOD',
    'REQUEST_TIME',
    'REQUEST_TIME_FLOAT',
    'QUERY_STRING',
    'DOCUMENT_ROOT',
    'HTTP_ACCEPT',
    'HTTP_ACCEPT_CHARSET',
    'HTTP_ACCEPT_ENCODING',
    'HTTP_ACCEPT_LANGUAGE',
    'HTTP_CONNECTION',
    'HTTP_HOST',
    'HTTP_REFERER',
    'HTTP_USER_AGENT',
    'HTTPS',
    'REMOTE_ADDR',
    'REMOTE_HOST',
    'REMOTE_PORT',
    'REMOTE_USER',
    'REDIRECT_REMOTE_USER',
    'SCRIPT_FILENAME',
    'SERVER_ADMIN',
    'SERVER_PORT',
    'SERVER_SIGNATURE',
    'PATH_TRANSLATED',
    'SCRIPT_NAME',
    'REQUEST_URI',
    'PHP_AUTH_DIGEST',
    'PHP_AUTH_USER',
    'PHP_AUTH_PW',
    'AUTH_TYPE',
    'PATH_INFO',
    'ORIG_PATH_INFO') ;

echo '<table cellpadding="10">' ;
foreach ($indicesServer as $arg) {
    if (isset($_SERVER[$arg])) {
        echo '<tr><td>'.$arg.'</td><td>' . $_SERVER[$arg] . '</td></tr>' ;
    }
    else {
        echo '<tr><td>'.$arg.'</td><td>-</td></tr>' ;
    }
}
echo '</table>' ;