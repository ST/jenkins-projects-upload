<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jenkins test uploader</title>

    <!-- Styling -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/uber/header.css">

    <link rel="stylesheet" href="css/uber/template.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    {block name=head}{/block}
</head>
<body>


<header class="wrap uber-header header-4 light-color">
    <div class="container">
        <div class="row">

            <div class="col-xs-3 col-sm-3">
                <div class="logo-image">
                    <a href="/" title="SIB, a crucial link in the life science chain">
                        <img class="logo-img" src="http://www.isb-sib.ch/images/sib/SIB_LogoQ_GBv.png"
                             alt="SIB, a crucial link in the life science chain"/>
                        <span>SIB, a crucial link in the life science chain</span>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 text-center">
                <div>
                    <h3>Jenkins Test Uploader</h3>
                </div>
            </div>

        </div>
    </div>
    {if $user.cn}
    <span style="margin-right: 15px;" class="pull-right">Hello {$user.cn}

            <a href="login">logout</a>
    </span>
    {/if}
</header>
{if $user.cn}
<span style="margin-right: 15px;" class="pull-right">
{if $user.isadmin}
    <a href="admin">admin</a>&nbsp;:&nbsp;
{/if}
    <a href="upload">upload</a>
</span>
{/if}

{block name=body}{/block}

</body>
</html>
