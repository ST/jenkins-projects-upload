<?php

/**
 * Created by PhpStorm.
 * User: partimo
 * Date: 11/02/16
 * Time: 16:53
 */

namespace Vital\User;

class User
{

    private $config = array();
    // what method to use to check the auth credentials, default ldap
    private $service = "ldap";
    private $logger;
    private $timeout;
    private $isLoggedIn;
    private static $user;
    private static $user_projects;
    /* = array(
        'partimo' => array('coev' => 'Coev','expasy' => 'ExPASy'),
        'vioannid' => array('coev' => 'Coev', 'embnet' => 'EMBnet-Tools','expasy' => 'ExPASy', 'va-test' => 'Vava-test'),
        'sduvaud' => array('coev' => 'Coev', 'oma' => 'OMA', 'bgee' => 'BGEE', 'prosite' => 'Prosite', 'string' => 'STRING', 'nextprot' => 'neXtProt', 'viralzone' => 'ViralZone','expasy' => 'ExPASy', 'arraymap-beacon' => 'arrayMap-Beacon'),
        'hstockin' => array('coev' => 'Coev','expasy' => 'ExPASy', 'arraymap-beacon' => 'arrayMap-Beacon'),
        'awaterho' => array('swiss-model' => 'SWISS-MODEL'),
	'jhaas' => array('cameo' => 'CAMEO'),
        'dteixeir' => array('nextprot' => 'neXtProt', 'sparql-playground' => 'Sparql Playground')
    );
*/
    public function __construct($config, $logger)
    {

        \FB::log("initialising User");
        \FB::log($config);
        \FB::log($logger);

        User::$user_projects = include '../config/users.php';
        $this->isLoggedIn = false;
        $this->config = $config;
        $this->timeout = $config['timeout'] + time();
        $this->logger = $logger;

        // get user from session
        User::$user = $_SESSION['user'];
        \FB::log(User::$user, "user in user");

    }


    public static function get_users_projects($username=null){

        if(!$username){
            $username = user::get_current_username();
        }

        \FB::log($username);

        if(isset(User::$user_projects[$username])){
            return User::$user_projects[$username];
        }

        return false;

    }

    public static function get_current_user($key=null)
    {

        if(isset($key) and key_exists($key, User::$user)) {
            return User::$user[$key];
        }
        
        return User::$user;

    }

    public static function get_current_user_id()
    {

        return User::$user['user']['id'];

    }

    public static function get_current_username()
    {

        return User::$user['user']['username'];

    }

    private static function getPermissionValue()
    {
        \FB::error(func_get_args(), "args in getPermissionValue");
        // set permissionvalues, add permission levels to arguments and get master permission value
        $val = 0;
        foreach (func_get_args() as $flag) {
            \FB::error($flag, "foreach flag");
            if (is_array($flag)) {
                foreach ($flag as $f) {
                    $val = $val | User::getPermissionValue($f);
                }
            } else {

                $val = $val | $flag;

            }

            \FB::error($val, "foreach val");
        }

        \FB::error($val, "getPermissionValue val");
        return $val;

    }


    public static function has_permission($val, $permission)
    {

        // does permission level include permission

        $val = (int)$val;
        $permission = (int)$permission;

        return (($val & $permission) === $permission);

    }

    public function check_status()
    {

        if (empty($_SESSION['user']) || @$_SESSION['user']['expires'] < time()) {

            return false;

        } else {

            return true;

        }

    }

}
