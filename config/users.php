<?php

return array(
        'partimo' => array('coev' => 'Coev','expasy' => 'ExPASy'),
        'vioannid' => array('coev' => 'Coev', 'embnet' => 'EMBnet-Tools','expasy' => 'ExPASy', 'va-test' => 'Vava-test', 'oma' => 'OMA', 'MetaPIGA' => 'MetaPIGA'),
        'sduvaud' => array('finding-people' => 'finding-people', 'coev' => 'Coev', 'oma' => 'OMA', 'bgee' => 'BGEE', 'prosite' => 'Prosite', 'string' => 'STRING', 'nextprot' => 'neXtProt', 'viralzone' => 'ViralZone','expasy' => 'ExPASy', 'arraymap-beacon' => 'arrayMap-Beacon', 'benchmark' => 'Benchmark', 'raxml' => 'RAxML', 'MetaPIGA' => 'MetaPIGA'),
        'hstockin' => array('coev' => 'Coev','expasy' => 'ExPASy', 'raxml' => 'raxml', 'MetaPIGA' => 'MetaPIGA'),
        'awaterho' => array('swiss-model' => 'SWISS-MODEL'),
        'jhaas' => array('cameo' => 'CAMEO'),
        'dteixeir' => array('nextprot' => 'neXtProt', 'sparql-playground' => 'Sparql Playground'),
        'ddylus' => array('phylo-io' => 'Phylo.io')
);

