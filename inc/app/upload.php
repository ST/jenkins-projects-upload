<?php
/**
 * User: partimo
 * Date: 28/07/15
 * Time: 14:39
 */

/**
 * @param $app
 * @return mixed
 * @throws Exception
 */
function upload($app)
{

    //\FB::log($app['params'], "params1");

    $data = array();

    $params = array_map('strip_tags', $_POST);
    $app['params'] = array_merge($params, $app['params']);

    //\FB::log($app['params'], "params2");

    $user = new Vital\User\User($app['settings'], $app['logger']);


    if(!$_SESSION['authenticated']) {
        $data['tpl.data']['greeting'] = "Protected page";
        $data['tpl.data']['template'] = 'login';
        $data['tpl.data']['title'] = 'Login with your SIB userid';
        $data['tpl.data']['loginreason'] = "Page is protected.";
        return $data;
    }


    $data['tpl.data']['template'] = "upload";
    $data['tpl.data']['userprojects'] = $user::get_users_projects();


    if (!isset($_POST['submit'])) {
        $data['tpl.data']['showform'] = true;
        $data['tpl.data']['template'] = "upload";
    } else {

        $upload_status = handle_upload($data['tpl.data']['userprojects'], $app['settings']['uploaddir']);

        if ($upload_status['success']) {

            $data['tpl.data']['success'] = $upload_status['successmessages'];
            $data['tpl.data']['showconfirm'] = true;
            $data['tpl.data']['showform'] = false;

        } else {

            $data['tpl.data']['errors'] = $upload_status['errormessages'];
            $data['tpl.data']['showform'] = true;

        }

    }

    FB::log($data);

    return $data;

}

function handle_upload($userprojects, $upload_dir){

    // $upload_dir = "/opt/lampp/htdocs/sandbox/jenkins-projects/tests_staged/";
    $testname = $_POST['project'];
    $test_path = $upload_dir.$testname;

    FB::log($_POST, "post");
    FB::log($_FILES, "files");
    FB::log($_SESSION, "session");
    FB::log($userprojects, "userprojects");
    FB::log($testname, "testname");
    FB::log($upload_dir, "uploaddir");
    FB::log($test_path, "test path");

    $is_allowed_project = isset($userprojects[$testname]);

    if($_FILES["testfiles"]["name"][0] == "" || $_FILES["testfiles"]["name"][1] == "") {
        $messages['errormessages'][] = "You must upload test and ini file";
    } else if(trim($testname) == "" || (!$is_allowed_project && !$_SESSION['user']['isadmin'])){
        $messages['errormessages'][] = "Project was not selected or not allowed";
    } else {

        $php_syntax_check = true;
        $php_err = false;
        $ini_err = false;
        $php_done = false;
        $python_done = false;
        $ini_done = false;
        $file_index = 0;

        foreach ($_FILES["testfiles"]["error"] as $key => $error) {

            // no upload errors
            if ($error == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["testfiles"]["tmp_name"][$key];
                $name = $_FILES["testfiles"]["name"][$key];

                FB::log("Checking filetype for file " . $name . " (" . $tmp_name . ")");

                // is it an ini file
                if ((strpos(shell_exec('file ' . $tmp_name), "ASCII text") || strpos(shell_exec('file ' . $tmp_name), "ASCII English text")) &&
                    (!strpos(shell_exec('file ' . $tmp_name), "PHP script" && !strpos(shell_exec('file ' . $tmp_name), "Python script"))) && !$ini_err && !$ini_done)
                    {

                    // plain ascii, try to load the array
                    $test_ini = parse_ini_file($tmp_name);

                    if (!$php_syntax_check || !is_array($test_ini) || empty($test_ini) ||
                        !isset($test_ini['contact_mail_log']) ||
			            !isset($test_ini['job_description']) ||
			            !isset($test_ini['job_title']) ||
                        !isset($test_ini['test_recurrence'])
                    ) {


                        FB::error(shell_exec('file ' . $tmp_name));
                        $messages['errormessages'][] = "File " . $name . " was not proper ini or test file";
                        $ini_err = true;

                    } else {

                        $name = "suite.ini";
                        $ini_done = true;

                    }


                } else if (strpos(shell_exec('file ' . $tmp_name), "PHP script") && !$php_err & !$php_done) {
                    // check its syntax is correct
                    $php_syntax_check = exec('php -l ' . $tmp_name, $output, $status);

                    FB::log($php_syntax_check, "php_syntax_check");
                    FB::log($output[1], " output 1");

                    if ($status != 0) {

                        FB::error("php syntax error detected");

                        if (strpos($output[1], "on line")) {

                            $error_line_msg = implode(" ", array_slice(explode(" ", $output[1]), -3, 3));

                        }

                        $messages['errormessages'][] = "Syntax error in " . $name . " " . $error_line_msg . " - upload aborted";
                        $php_err = true;

                    } else {

                        $name = $testname . ".php";
                        $php_done = true;

                    }

                } else if (strpos(shell_exec('file ' . $tmp_name), "Python script")){
                    
                    $name = $testname . ".py";
                    $python_done = true;
                    
                } else {

                    if(!php_done && !$ini_done && !$python_done) {
                        $messages['errormessages'][] = "Filetype for " . $name . " was not allowed";
                    }

                }

                $names[$file_index]['tmp_name'] = $tmp_name;
                $names[$file_index]['name'] = $name;
                $file_index++;

            } else {

                FB::error("upload error");
                FB::error($error);
                $messages['errormessages'][] = "Files were not uploaded, server rejected the file. ";

            }

        }

        if($ini_done && ($php_done || $python_done)){

            FB::info($names);

            FB::log("creating test dir " . $test_path);

            if (!is_dir($test_path) && !mkdir($test_path, 0777, true)) {
                FB::error("Uploading of the files was not successful, could not create test directory.");
            } else {

                chmod($test_path, 0777);

                foreach ($names as $file) {

                    $name = $file['name'];
                    $tmp_name = $file['tmp_name'];

                    // not the safest option - should we not owerwrite and inform existing test hasn't been committed?
                    // user should be anble to upload whatever they want though so leaving for now
                    if (file_exists($test_path . "/" . $name)) {
                        chmod($test_path . "/" . $name, 0755); //Change the file permissions if allowed
                        unlink($test_path . "/" . $name); //remove the file
                    }

                    FB::log("moving " . $test_path . "/" . $name);
                    if (move_uploaded_file($tmp_name, $test_path . "/" . $name)) {

                        chmod($test_path . "/" . $name, 0777);
                        FB::log("uploaded file to ".$test_path . "/" . $name);

                    } else {


                    }

                }

            }

        } else {

            $messages['errormessages'][] = "You have to upload a test- and ini-file";

        }

    }

    // TODO kludge
    if(!is_array($messages['errormessages'])){
        //we know when was the last time this user uploaded this test
        //update_updater($test_path, $_SESSION['user']['user']['username']);
        $messages['success'] = true;
        $messages['successmessages'][] = "Files were uploaded successfully. Please allow 24 hours for your test to appear in 
        Jenkins.";
        mail("panu.artimo@sib.swiss,vassilios.ioannidis@sib.swiss,heinz.stockinger@sib.swiss,severine.duvaud@sib.swiss", "New Selenium test uploaded for ".$testname." project.", "New testsuite has been uploaded for $testname project by ". $_SESSION['user']['user']['username'].". \nPlease check and approve it at https://www.expasy.org/web-tests/admin\n
        (these emails are going to another address at some point...)");
    }

    FB::info($messages);

    return $messages;
}

function update_updater($test, $user){

    touch($test . "/" . $user);
    return;

}
