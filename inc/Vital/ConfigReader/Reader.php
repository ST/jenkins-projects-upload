<?php

namespace Vital\ConfigReader;

class Reader
{
    private $filename;

    public function __construct($filename)
    {
        $this->filename = $filename;

    }

    public function Load()
    {
        $format = $this->getFileFormat();

        if (!$this->filename || !$format) {
            throw new \RuntimeException('A valid configuration file must be passed before reading the config.');
        }

        if (!file_exists($this->filename)) {
            throw new \InvalidArgumentException(
                sprintf("The config file '%s' does not exist.", $this->filename));
        }

        if ('php' === $format) {
            $config = require $this->filename;
            $config = (1 === $config) ? array() : $config;
            return $config ?: array();
        }

        if ('json' === $format) {
            $config = $this->parseJson($this->filename);
            return $config ?: array();
        }

        throw new \InvalidArgumentException(
                sprintf("The config file '%s' appears has invalid format '%s'.", $this->filename, $format));
    }

    private function parseJson($filename)
    {
        $json = file_get_contents($filename);
        $json = $this->processRawJson($json);
        return json_decode($json, true);
    }

    protected function processRawJson($json)
    {
        return $json;
    }

    public function getFileFormat()
    {
        $filename = $this->filename;

        if (preg_match('#.json(.dist)?$#i', $filename)) {
            return 'json';
        }

        if (preg_match('#.php(.dist)?$#i', $filename)) {
            return 'php';
        }

        return pathinfo($filename, PATHINFO_EXTENSION);
    }
}