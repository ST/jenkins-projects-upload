<?php
/************************ LICENCE ***************************
 *     This file is part of <jenkins-projects-upload>
 *     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *****************************************************************/
/**
 * File index.php created on 08/08/16 11:52 by partimo
 * Modified by:
 */

session_start();

//require('../config/config.php');

require('../vendor/autoload.php');
require('../inc/app.php');

FB::log($_SESSION, "session in index");
FB::log($_SERVER['REQUEST_URI']);

// ROUTES
$router = new AltoRouter();

// this should work when doc root is not a sub directory
$router->setBasePath(DOC_ROOT);

$router->map('GET','/login', 'login');
$router->map('GET','/admin', 'admin');
$router->map('POST','/admin/[a:dir]?/[a:file]?', 'admin');
$router->map('POST','/admin', 'admin');
$router->map('POST','/upload', 'upload');
$router->map('GET','/upload', 'upload');
$router->map('GET','/', 'upload');


// match current request url
$match = $router->match();
FB::info($match, "match in index");


$app['params'] = $match['params'];
FB::info($app['params'], "params in index");

$is_allowed = no_auth_required($app, $match['target']) || is_user_authenticated($app);
FB::info($is_allowed, "is_allowed");

// require our controller
if(is_array($match) && file_exists(__DIR__."/../inc/app/".$match['target'].".php")  && $match['target'] != "login" && $is_allowed) {

    require(__DIR__.'/../inc/app/'.$match['target'].'.php');
    FB::info("included ".__DIR__.'/../inc/app/'.$match['target'].'.php');

}

// call closure or throw 404 status
// data will have tpl and tpl.data
if($match && is_function($match['target'])) {
    FB::info("match and function was found");
    FB::info($app, "called ".$match['target']." with app");
    $data = call_user_func($match['target'], $app);
} else {
    // no route was matched or route asked was not allowed
    $data = array();

    $data['tpl.data']['target'] = $match['target'];

    //if($match['target'] == "upload"){
    if(!$is_allowed || $match['target'] == "login"){

        $data['tpl.data']['greeting'] = "Protected page";
        $data['tpl.data']['template'] = 'login';
        $data['tpl.data']['title'] = 'Login with your SIB userid';
        $data['tpl.data']['target'] = 'upload';

        if(!isset($data['tpl.data']['loginmessage'])) {
            $data['tpl.data']['loginreason'] = "Page is protected.";
        }

    } else {

        $data['tpl.data']['loginerror'] = "Page not found";
        FB::error("did not match any routes, or the action was not available or not allowed.");

    }

}

$data['tpl.data']['user'] = $_SESSION['user']['user'];

FB::info($data, "data");

if(isset($data['tpl.data']['loginerror'])){
    $data = show_no_path_error($data);
}

$template = isset($data['tpl.data']['template']) ? $data['tpl.data']['template'] : LOGIN_TPL;
FB::info($template, "template before render");

if($template == LOGIN_TPL) {
    destroy_session();
}

$response = $app['tpl']->render($data, $template);

echo $response;
