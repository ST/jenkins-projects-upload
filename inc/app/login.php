<?php
/**
 * Created by PhpStorm.
 * User: partimo
 * Date: 15/02/16
 * Time: 13:07
 */

function login($app) {

    $data['tpl.data']['template'] = 'login';
    $data['tpl.data']['loginmessage'] = 'Login with your intranet userid';
    $data['tpl.data']['loginerror'] = false;

    // not used for now
    if(isset($_SESSION['loginreason'])){
        $data['tpl.data']['loginmessage'] = implode(", ", $_SESSION['loginreason']);
        $data['tpl.data']['loginerror'] = true;
        FB::info("Wrong login: ".$_SESSION['loginreason']);
        unset($_SESSION['loginmessage']);
        unset($_SESSION['loginreason']);
    }

    return $data;
}