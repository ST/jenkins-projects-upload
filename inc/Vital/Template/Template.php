<?php
namespace Vital\Template;

// todo, get cache info: is cached, should tpl be cached etc.
Class Template {

    private $tpl;
    private $vars = array();
    private $lang = null;
    private $templates = array();
    private $logger;
    private $theme;
    private $is_ajax;


    /**
     *
     * @constructor
     *
     * @access public
     *
     * @return void
     *
     */

    public function __construct($lang="en", $tplEngine, $logger, $theme="default", $is_ajax = 0) {

        $this->lang = $lang;
        $this->tpl  = $tplEngine;
        $this->theme = $theme;
        $this->logger = $logger;
        $this->is_ajax = $is_ajax;
        \FB::log("is_ajax: ".$is_ajax);
        //$logger->addInfo("Template initialised to logger");
        //$logger->addError("this is error and goes to logfile : Template initialised to logger");
//        \FB::log("Template initialised");

    }

    /**
     *
     * @add templates that need to be included in the main template either inline or otherwise
     *
     * @param array $template name, inline true/false
     *
     */
    public function add_template($template){
        if (!isset($this->templates[$template['name']])) {
            $this->templates[$template['name']] = array();
        }
        foreach ($template as $key => $value) {
            $this->templates[$key][] = $value;
        }

        FB::log("added template: ".$key." to templates");
        FB::log($this->templates);
    }
    /**
     *
     * @set undefined vars
     *
     * @param string $index
     * @param mixed $value
     * @return void
     *
     */
    public function __set($index, $value) {
        $this->vars[$index][] = $value;
        \FB::log("added: ".$value." to vars ".$index);
    }

    function getLang(){

        return $this->lang;

    }

    /**
     * @param string $name       name of the template to be rendered
     * @param array  $data       all data to be assigned to the template
     */
    public function render($alldata, $name, $full = true) {

        $data = $alldata['tpl.data'];

        \FB::info($this->theme, "theme in template");
        $this->tpl->assign("theme", $this->theme);
        \FB::info($data, "data in template");

        // get the first template directory
        $path = $this->tpl->template_dir[0].$name . '.html';
        \FB::info("using template: ".$path);
        if (file_exists($path) == false) {
            \FB::error('Template '.$name.' not found in ' . $path);
            return false;
        }

        $this->tpl->assign($data);

        // get all "extra templates"
        if(isset($data['extratemplate']) && is_array($data['extratemplate'])){
            \FB::info($data['extratemplate']);
            foreach ($data['extratemplate'] as $extratemplate) {

                foreach ($extratemplate as $tname => $inline) {

                    if($inline){
                        $this->tpl->assign($tname, $this->tpl->fetch($tname.'.html'));

                    }

                }

            }

        }

        $this->tpl->assign("display", "full");

        if(!$this->is_ajax && $full){
            \FB::info("Display full page");
            $this->tpl->assign("display", "full");
            return $this->tpl->fetch('extends:layout.tpl|'.$name.'.html');
        } else {
            \FB::info("Display ajax or plain page");
            $this->tpl->assign("display", "ajax");
            return $this->tpl->fetch($name.'.html');
        }

    }

    public static function is_ajax() {

        return template::$is_ajax;

    }

}
?>