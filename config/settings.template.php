<?php

return array(
    "lang" => "en",

    'tpl'          => array(
        'template_dir' => ROOT.'/templates/default/',
        'compile_dir'  => ROOT.'/templates/templates_c/',
        'config_dir'   => ROOT.'/templates/configs/',
        'cache_dir'    => ROOT.'/templates/cache/',
        'theme'        => ROOT.'default'
    ),

    'ldap'         => array(
        'host'         => 'ldap.vital-it.ch',
        'port'         => '389',
        'version'      => 3,
        'basedn'       => 'o=SIB',
        'userattr'     => 'uid',
        'realm'        => 'Authentication required'
    ),

    'timeout'      => 86400,
    // array of pages/controllers that are accessible without loggin in,
    // "all" to disable authentication
    // TODO make it possible to use ! to negate the authentication
    'allowed'      => array('login'),
    'loginpage'    => array('/login'),
    'protectedscript' => array(1),
    // local
    //'uploaddir' => "/opt/lampp/htdocs/sandbox/jenkins-projects/tests_staged/",
    //'testsdir' => "/opt/lampp/htdocs/sandbox/jenkins-projects/tests/",
    // server, filesystem paths to test related directories
    'uploaddir' => "/home/local/jenkins/jenkins-projects-upload/tests_staged/",
    'testsdir' => "/home/local/jenkins/jenkins-projects/tests/",
    'jenkinstestserver' => "jenkins.vital-it.ch",
    'log_dir'           => ROOT.'/log/',
    'user_projects'     => array(
        'partimo' => array('coev' => 'Coev','expasy' => 'ExPASy'),
        'vioannid' => array('coev' => 'Coev', 'embnet' => 'EMBnet-Tools','expasy' => 'ExPASy', 'va-test' => 'Vava-test'),
        'sduvaud' => array('coev' => 'Coev', 'oma' => 'OMA', 'bgee' => 'BGEE', 'prosite' => 'Prosite', 'string' => 'STRING', 'nextprot' => 'neXtProt', 'viralzone' => 'ViralZone','expasy' => 'ExPASy', 'arraymap-beacon' => 'arrayMap-Beacon'),
        'hstockin' => array('coev' => 'Coev','expasy' => 'ExPASy'),
        'awaterho' => array('swiss-model' => 'SWISS-MODEL'),
        'jhaas' => array('cameo' => 'CAMEO'),
        'dteixeir' => array('nextprot' => 'neXtProt', 'sparql-playground' => 'Sparql Playground')
    )
    
);
