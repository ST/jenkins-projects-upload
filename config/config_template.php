<?php

error_reporting(E_ALL ^ E_NOTICE);

defined('PRODUCTION') or define('PRODUCTION', 0);
defined('DEBUGGING') or define('DEBUGGING', 1);

// defined('LDAP_DEBUGGING') or define('LDAP_DEBUGGING', 1);

// locahost test, at root
// define('DOC_ROOT', "");

// URL to access the script on LOCAL
define('DOC_ROOT', "/jenkins-projects-upload");

// URL to access the script on SERVER
//define('DOC_ROOT', "/web-tests");

// filesystem path for the app root
define('ROOT', "/opt/lampp/htdocs/sandbox/jenkins-projects-upload");
define('LOGIN_TPL', "login");

// uncomment when LDAP is used to get groups etc.
/*
$secure = ($_SERVER['HTTPS'] == "on") ? "s" : "";

if(LDAP_DEBUGGING === 1) {
    define('FINDING_PEOPLE_URL', 'http'.$secure.'://test-www-sib-swiss.vital-it.ch/fp/people_search.php');
} else if(PRODUCTION === 0){
    // test instance
    define('FINDING_PEOPLE_URL', 'http'.$secure.'://'.$_SERVER['SERVER_NAME'].'/fp/people_search.php');
} else {
    // prod
    define('FINDING_PEOPLE_URL', 'http'.$secure.'://'.$_SERVER['SERVER_NAME'].'/fp/people_search.php');
}
*/
