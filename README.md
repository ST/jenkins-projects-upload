# Jenkins test uploader

With Jenkins test uploader a user can upload Selenium test files and related settings files to testing server. The same tool allows administrator users to check sent tests and approve them to be run automatically by the Jenkins server.

Requirements
  - SIB LDAP account
  - A project to be tested added to the interface and linked to users ldap account
  
## Uploading
A test consists of 2 files the user has to provide: 
a PHP script written for phpunit test framework using Facebook's selenium webdriver library, and a suite.ini file which contains information about the test to be used in Jenkins' user interface.
  - use LDAP username and password to login at https://www.expasy.org/web-tests/
  - select the project you want to upload test to from the **project name** dropdown
  - use the browse buttons to select a test file (PHP script with php extension) and a suite.ini for uploading
  - press **upload** button
  - You will be notified of succesful upload or any problems that might have happened
  - Administrators are notified about your uploaded test to further process it
  
The user has access to a _starter pack_ which is a self contained test environment with example test runnable on local machine

We may extend selection of allowed scripting languages in the future, but we start with PHP.

## Installation 
 - create a directory for the files.
 - clone the project into that directory
 - **git clone git@gitlab.isb-sib.ch:ST/jenkins-projects-upload.git .**
 - note the . at the end
 - you also need to clone the project git@gitlab.isb-sib.ch:ST/jenkins-projects.git where tests will be committed to another root level directory
 - create an Apache alias pointing webroot to /path/to/cloneddir/htdocs directory (i.e. /home/local/jenkins/jenkins-projects-upload/htdocs)
 - you might need to update all components with 'composer update, composer install' in the root directory (at least to obtain the directory 'vendor')
 - create the file 'config/config.php' based on the template in the same directory and configure it
 - configure the file 'config/settings.php' to specify the path of jenkins-projects.git
 - modify .htaccess file in that directory to have it's RewriteBase setting pointing to the URL you are planning to use. i.e., RewriteBase /web-tests
 - create the directory 'tests_staged' and give apache user write permission
 - check $jenkins_job_path and $test_directory variables point to correct location in file config/settings.php
 - restart Apache
