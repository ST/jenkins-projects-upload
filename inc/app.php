<?php
/************************ LICENCE ***************************
 *     This file is part of <jenkins-projects-upload>
 *     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *****************************************************************/
/**
 * File app.php created on 08/08/16 10:00 by partimo
 * Modified by:
 */

include('../config/config.php');
include('../config/settings.php');

FB::setEnabled(false);
if(DEBUGGING === 1){
    FB::setEnabled(true);
}


require('../inc/functions.php');
use Pimple\Container;

// our DIC
$app = new Container();

// logging
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


// Create the logger, writes log file
$app['logger'] = function($c) {
    return new Logger('log');
};

// lang object
$app['lang.service'] = function($c) {
    return new \Vital\ConfigReader\Reader(__DIR__."/../config/lang.php");
};

// settings
$app['settings.service'] = function($c) {
    return new \Vital\ConfigReader\Reader(__DIR__."/../config/settings.php");
};

$app['lang.str'] = function($c) {
    $langService = $c['lang.service'];
    return $langService->Load();
};

$app['settings'] = function($c) {
    FB::info($c['settings.service']);
    $settingsService = $c['settings.service'];
    return $settingsService->Load();
};


FB::info($app);

check_dirs($app);

$app['tpl.engine'] = function($app) {
    $tpl = new Smarty();

    if(is_array($app['settings']['tpl'])) {
        $tpl->template_dir = $app['settings']['tpl']['template_dir'];
        $tpl->compile_dir  = $app['settings']['tpl']['compile_dir'];
        $tpl->config_dir   = $app['settings']['tpl']['config_dir'];
        $tpl->cache_dir    = $app['settings']['tpl']['cache_dir'];
    } else {
        $tpl->template_dir = '../templates/default/';
        $tpl->compile_dir  = '../templates/templates_c/';
        $tpl->config_dir   = '../templates/configs/';
        $tpl->cache_dir    = '../templates/cache/';
    }


    FB::error($tpl, "tpl");
    FB::info($tpl->config_dir, "smarty configs");

    FB::info('Setting tpl langstring to '.$app['settings']['lang']);
    $tpl->assign($app['lang.str']['langstring'][$app['settings']['lang']]);

    return $tpl;
};

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $is_ajax = 1;
} else {
    $is_ajax = 0;
}

$app['tpl'] = new \Vital\Template\Template($app['settings']['lang'], $app['tpl.engine'], $app['logger'], $app['settings']['tpl']['theme'], $is_ajax);


// Now add some handlers for logging
// firephphandler doesn't give flexibility as objects can't be logged anyway. Use Firephp directly as FB::log("message)
// it handles it.
//$app['logger']->pushHandler(new FirePHPHandler());
$app['logger']->pushHandler(new StreamHandler(ROOT.'/log/app.log', Logger::WARNING));

$logger = $app['logger'];
